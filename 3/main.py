import pygeocoder

# Masukkan nomor HP
phone_number = "+6281234567890"

# Cari alamat IP dari nomor HP
ip_address = pygeocoder.get_location(phone_number)

# Cetak alamat IP
print(ip_address.ip)
